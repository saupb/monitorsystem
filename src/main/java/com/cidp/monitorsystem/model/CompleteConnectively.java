package com.cidp.monitorsystem.model;

import java.util.Objects;

/**
 * @date 2021/4/4 -- 16:09
 **/
public class CompleteConnectively {
    private String sip;
    private String dip;
    private String sifindex;
    private String difindex;

    public String getSip() {
        return sip;
    }

    public void setSip(String sip) {
        this.sip = sip;
    }

    public String getDip() {
        return dip;
    }

    public void setDip(String dip) {
        this.dip = dip;
    }

    public String getSifindex() {
        return sifindex;
    }

    public void setSifindex(String sifindex) {
        this.sifindex = sifindex;
    }

    public String getDifindex() {
        return difindex;
    }

    public void setDifindex(String difindex) {
        this.difindex = difindex;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CompleteConnectively that = (CompleteConnectively) o;
        return (Objects.equals(sip, that.dip) &&
                Objects.equals(dip, that.sip) )||
                (Objects.equals(sip, that.sip) &&
                        Objects.equals(dip, that.dip));
    }

    @Override
    public String toString() {
        return "CompleteConnectively{" +
                "sip='" + sip + '\'' +
                ", dip='" + dip + '\'' +
                ", sifindex='" + sifindex + '\'' +
                ", difindex='" + difindex + '\'' +
                '}';
    }

    @Override
    public int hashCode() {
        return 1;
    }
}
