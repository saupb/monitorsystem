package com.cidp.monitorsystem.mapper;

import com.cidp.monitorsystem.model.CompleteConnectively;
import com.cidp.monitorsystem.model.Connectively;
import com.cidp.monitorsystem.model.Link;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Set;

public interface ConnectivelyMapper {
    List<Connectively> getConnect();

    Connectively hsaSip2Dip(@Param("sip") String sip, @Param("dip") String dip);
    Connectively hasDip2Sip(@Param("dip") String dip,@Param("sip") String sip);

    void addConnect(Connectively connectively);

    List<Connectively> getTrimConnect();

    List<CompleteConnectively> selectConnectively();

    Set<CompleteConnectively> selectConnectivelySet();
    int clearConnectively();

    int insertConnectively(@Param("list") List<CompleteConnectively> connectivelies);

    List<Link> selectAllLinks();

    Link selectIntfaceFlowByIpAndIfIndex(@Param("ifindex") String ifindex,@Param("ip") String ip);

    String selectDifByDIpAndDIfindex(@Param("dip") String dip,@Param("difindex") String difindex);

    Set<Link> selectAllLinksSet();
}
