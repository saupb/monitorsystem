package com.cidp.monitorsystem;
import com.cidp.monitorsystem.mapper.InterfaceOfMacMapper;
import com.cidp.monitorsystem.model.Connectively;
import com.cidp.monitorsystem.model.Link;
import com.cidp.monitorsystem.service.CpuService;
import com.cidp.monitorsystem.service.DiskService;
import com.cidp.monitorsystem.service.SystemService;
import com.cidp.monitorsystem.service.dispservice.ConnectivelyService;
import com.cidp.monitorsystem.topology.TopologyDiscovery;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.scheduling.annotation.EnableAsync;

import java.util.List;


@SpringBootTest
@EnableAsync
class MonitorsystemApplicationTests {
   @Autowired
    TopologyDiscovery topologyDiscovery;
   @Autowired
    InterfaceOfMacMapper macMapper;
   @Autowired
   SystemService systemService;
   @Autowired
   CpuService cpuService;
   @Autowired
   DiskService diskService;
   @Autowired
    ConnectivelyService connectivelyService;
    @Test
    void contextLoads() throws Exception {
//        topologyDiscovery.connectivelyOfL2ToL2();
//        topologyDiscovery.connectOfAll();
//        systemService.deviceSearchByips(systemService.getAllActDevice());
//          cpuService.GetInfo("192.168.3.108");
//        diskService.GetDiskInfo();
//        topologyDiscovery.deviceMacForward();
//        topologyDiscovery.deviceInterfaceMac();
//        topologyDiscovery.indexRelatePort();
//        topologyDiscovery.neighborMac();
        connectivelyService.selectAllLinks();
    }

}
